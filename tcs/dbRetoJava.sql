-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: reto
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departamento` (
  `IdDepartamento` int NOT NULL,
  `Nombre` varchar(225) NOT NULL,
  `Descripcion` varchar(225) NOT NULL,
  `Codigo` int DEFAULT NULL,
  PRIMARY KEY (`IdDepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento` VALUES (1,'RRHH','Recursos humano',1),(2,'Tecnologia','Departamento Sistemas',2);
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empleado` (
  `IdEmpleado` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(225) NOT NULL,
  `Apellidos` varchar(225) NOT NULL,
  `NumeroDocumento` varchar(225) NOT NULL,
  `Correo` varchar(225) NOT NULL,
  `Telefono` varchar(225) NOT NULL,
  `Activo` tinyint(1) DEFAULT NULL,
  `Salario` float DEFAULT NULL,
  `IdDepartamento` int NOT NULL,
  PRIMARY KEY (`IdEmpleado`),
  UNIQUE KEY `UNIQUE_EMPLEADO` (`IdEmpleado`,`NumeroDocumento`),
  UNIQUE KEY `NumeroDocumento_UNIQUE` (`NumeroDocumento`),
  UNIQUE KEY `IdEmpleado_UNIQUE` (`IdEmpleado`),
  KEY `IdDepartamento_idx` (`IdDepartamento`),
  CONSTRAINT `IdDepartamento_Empleado` FOREIGN KEY (`IdDepartamento`) REFERENCES `departamento` (`IdDepartamento`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (100,'Jurgen','Perez','1037640132','jurgen55@hotmail.com','3185570824',1,500,1),(101,'Mateo','Sanchez','34099090909','mateo@hotmail.com','3214567890',1,500,1),(103,'Camila','arboleda','1035755','santiago@hotmail.com','5555444',1,3000,1),(104,'Natalia','Vargas','1035735','nataliavargas@hotmail.com','3185533590',0,400,1),(105,'Alberto','Perez','1037640177','alberto@hotmail.com','3185574590',1,400,1),(106,'Jhon','Vargas','1037640167','jhon17@hotmail.com','31855708987',1,500000,1),(107,'Mario','Hernandez','1037689167','mario17@hotmail.com','31890708987',1,700000,2);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funciones`
--

DROP TABLE IF EXISTS `funciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `funciones` (
  `IdFunciones` int NOT NULL,
  `Nombre` varchar(225) NOT NULL,
  `Descripcion` varchar(225) NOT NULL,
  `IdDepartamento` int NOT NULL,
  PRIMARY KEY (`IdFunciones`),
  KEY `IdDepartamento_idx` (`IdDepartamento`),
  CONSTRAINT `IdDepartamento_Funciones` FOREIGN KEY (`IdDepartamento`) REFERENCES `departamento` (`IdDepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funciones`
--

LOCK TABLES `funciones` WRITE;
/*!40000 ALTER TABLE `funciones` DISABLE KEYS */;
INSERT INTO `funciones` VALUES (1,'Administrar base de datos','DBA',2),(2,'Emitir oferta laboral','Reclutar',1),(3,'Diseño de arquitectura aplicativo','Diseño de aplicativo',2),(4,'Desarrollo de aplicaciones moviles','Desarrollo moviles',2),(5,'Atender dudas asociados','Procesar dudas',1),(6,'Desarrollo de aplicaciones web','Developer web',2),(7,'Brigada de salud','Salud en el trabajo',1),(8,'Creacion de boletin mensual','Notificar sobre eventos en la compañia',1);
/*!40000 ALTER TABLE `funciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-30 18:15:49
