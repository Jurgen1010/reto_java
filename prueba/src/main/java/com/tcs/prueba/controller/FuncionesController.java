package com.tcs.prueba.controller;


import com.tcs.prueba.entity.DepartamentoEntity;
import com.tcs.prueba.entity.FuncionesEntity;
import com.tcs.prueba.service.FuncionesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class FuncionesController {

    @Autowired
    FuncionesService funcionesService;

    @GetMapping("/listFuncionByIdDepartamento")
    public List<FuncionesEntity> getFuncionesByDepartamento(@Validated @RequestBody DepartamentoEntity idDepartamento) {
        return funcionesService.findFuncionesByIdDepartamento(idDepartamento);
    }

    @PostMapping("/addFuncionToDepartamentoFromEmpleado")
    public ResponseEntity<List<FuncionesEntity>> addFuncion(@Validated @RequestBody List<FuncionesEntity>funcion, BindingResult result) {
        if (result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        List<FuncionesEntity> funcionCreate = funcionesService.addFuncionToEmpleado(funcion);
        return ResponseEntity.status(HttpStatus.CREATED).body(funcionCreate);
    }

}
