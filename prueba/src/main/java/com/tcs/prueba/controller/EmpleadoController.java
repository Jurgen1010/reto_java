package com.tcs.prueba.controller;

import com.tcs.prueba.entity.EmpleadoEntity;
import com.tcs.prueba.service.EmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class EmpleadoController {

    @Autowired
    EmpleadoService empleadoService;

    @GetMapping("/getEmpleados/{numeroDocumento}")
    public EmpleadoEntity getEmpleadoByNumeroDocumento(@PathVariable(value = "numeroDocumento") String numeroDocumento) {
        return empleadoService.findByNumeroDocumento(numeroDocumento);
    }

    @PostMapping("/addEmpleado")
    public ResponseEntity<EmpleadoEntity> addEmpleado(@Validated @RequestBody EmpleadoEntity empleado, BindingResult result) {
        if (result.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        EmpleadoEntity empleadoCreate = empleadoService.addEmpleado(empleado);
        return ResponseEntity.status(HttpStatus.CREATED).body(empleadoCreate);
    }

    @PutMapping("/activar_inactivarEmpleado/{status}/{numeroDocumento}")
    public ResponseEntity<EmpleadoEntity> activarOrInactivarEmpleado(@PathVariable(value = "status") String status,@PathVariable(value = "numeroDocumento") String numeroDocumento,@RequestBody EmpleadoEntity empleadoEntity){

        empleadoEntity.setNumeroDocumento(numeroDocumento);

        EmpleadoEntity empleadoDB = empleadoService.activarOrInactivarEmpleado(status,numeroDocumento,empleadoEntity);
        if (empleadoDB == null){
           return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(empleadoDB);
    }

}
