package com.tcs.prueba.controller;


import com.tcs.prueba.entity.DepartamentoEntity;
import com.tcs.prueba.service.DepartamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController//para indicarle que es un servicio rest
public class DepartamentoController {

    @Autowired
    DepartamentoService departamentoService;

    @GetMapping("/departamentos")
    public List<DepartamentoEntity> getAllDepartamentos(){
        return departamentoService.listAllDepartamentos();
    }


}
