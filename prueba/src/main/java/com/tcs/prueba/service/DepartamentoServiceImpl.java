package com.tcs.prueba.service;

import com.tcs.prueba.entity.DepartamentoEntity;
import com.tcs.prueba.repository.DepartamentoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DepartamentoServiceImpl implements DepartamentoService {

    /*
    Clase donde definire la implementacion de cada metodo que realizara mi servicio
     */
    @Autowired
    private DepartamentoRepository departamentoRepository;

    @Override
    public List<DepartamentoEntity> listAllDepartamentos() {
        return (List<DepartamentoEntity>) departamentoRepository.findAll();
    }

    @Override
    public DepartamentoEntity getDepartamento(Long id) {
        return departamentoRepository.findById(id).orElse(null);//En caso de no encontrarlo retornaremos un null
    }

    @Override
    public DepartamentoEntity crateDepartamento(DepartamentoEntity departamentoEntity) {
        /*
        Definir que realizara la implementacion
         */
        return null;
    }

    @Override
    public DepartamentoEntity deleteDepartamento(Long id) {
        /*
        Definir que realizara la implementacion
         */
        return null;
    }
}
