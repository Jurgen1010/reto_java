package com.tcs.prueba.service;

import com.tcs.prueba.entity.DepartamentoEntity;
import com.tcs.prueba.entity.EmpleadoEntity;
import com.tcs.prueba.entity.FuncionesEntity;
import com.tcs.prueba.repository.EmpleadoRepository;
import com.tcs.prueba.repository.FuncionesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class FuncionesServiceImpl implements FuncionesService {

    @Autowired
    private FuncionesRepository funcionesRepository;

    @Autowired
    private EmpleadoRepository empleadoRepository;


    @Override
    public List<FuncionesEntity> addFuncionToEmpleado(List<FuncionesEntity> funcionesEntity) {
       return funcionesRepository.saveAll(funcionesEntity);
    }

    @Override
    public List<FuncionesEntity> findFuncionesByIdDepartamento(DepartamentoEntity idDepartamento) {
        return funcionesRepository.findFuncionesByIdDepartamento(idDepartamento);
    }
}
