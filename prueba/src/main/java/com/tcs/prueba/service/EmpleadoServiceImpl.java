package com.tcs.prueba.service;

import com.tcs.prueba.entity.EmpleadoEntity;
import com.tcs.prueba.repository.EmpleadoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmpleadoServiceImpl implements EmpleadoService {

    @Autowired
    private EmpleadoRepository empleadoRepository;

    @Override
    public EmpleadoEntity findByNumeroDocumento(String numeroDocumento) {
        return empleadoRepository.findByNumeroDocumento(numeroDocumento);

    }

    @Override
    public EmpleadoEntity addEmpleado(EmpleadoEntity empleado) {
        return empleadoRepository.save(empleado);
    }

    @Override
    public EmpleadoEntity activarOrInactivarEmpleado(String status,String numeroDocumento,EmpleadoEntity empleadoEntity) {

        EmpleadoEntity empleadoDB = empleadoRepository.findByNumeroDocumento(numeroDocumento);
        if (null == empleadoDB) {
            return null;
        }
        boolean statusCreate = status.equalsIgnoreCase("ACTIVO");
        empleadoDB.setNombre(empleadoEntity.getNombre());
        empleadoDB.setApellidos(empleadoEntity.getApellidos());
        empleadoDB.setNumeroDocumento(empleadoEntity.getNumeroDocumento());
        empleadoDB.setCorreo(empleadoEntity.getCorreo());
        empleadoDB.setTelefono(empleadoEntity.getTelefono());
        empleadoDB.setActivo(statusCreate);
        empleadoDB.setSalario(empleadoEntity.getSalario());

        return empleadoRepository.save(empleadoDB);
    }


}
