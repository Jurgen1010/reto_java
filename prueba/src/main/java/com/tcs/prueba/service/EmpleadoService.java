package com.tcs.prueba.service;

import com.tcs.prueba.entity.EmpleadoEntity;

public interface EmpleadoService {
    public EmpleadoEntity findByNumeroDocumento(String numeroDocumento);

    public EmpleadoEntity addEmpleado(EmpleadoEntity empleado);

    public EmpleadoEntity activarOrInactivarEmpleado(String status, String numeroDocumento,EmpleadoEntity empleadoEntity);
}
