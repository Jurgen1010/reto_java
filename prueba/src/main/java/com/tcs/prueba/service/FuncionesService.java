package com.tcs.prueba.service;

import com.tcs.prueba.entity.DepartamentoEntity;
import com.tcs.prueba.entity.FuncionesEntity;
import java.util.List;


public interface FuncionesService {
    public List<FuncionesEntity> addFuncionToEmpleado(List<FuncionesEntity> funcionesEntity);
    public List<FuncionesEntity> findFuncionesByIdDepartamento(DepartamentoEntity idDepartamento);
}
