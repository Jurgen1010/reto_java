package com.tcs.prueba.service;

import com.tcs.prueba.entity.DepartamentoEntity;

import java.util.List;

public interface DepartamentoService {

    public List<DepartamentoEntity> listAllDepartamentos();
    public DepartamentoEntity getDepartamento(Long id);
    public DepartamentoEntity crateDepartamento(DepartamentoEntity departamentoEntity);
    public DepartamentoEntity deleteDepartamento (Long id);
}
