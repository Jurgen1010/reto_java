package com.tcs.prueba.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "departamento")
@Data
@AllArgsConstructor //para generar un constructor con todas las propiedades de nuestra entidad
@NoArgsConstructor //para generar un constructor sin argumentos de nuestra entidad
@Builder// me permitira crear nuestras instancias de mi entidad
public class DepartamentoEntity {
    @Id
    private Long idDepartamento;
    private String nombre;
    private String descripcion;
    private Long codigo;
}