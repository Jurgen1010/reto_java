package com.tcs.prueba.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "funciones")
@Data
@AllArgsConstructor @NoArgsConstructor @Builder
public class FuncionesEntity {
    @Id
    private Long idFunciones;
    private String nombre;
    private String descripcion;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdDepartamento")
    @JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
    private DepartamentoEntity idDepartamento;

}
