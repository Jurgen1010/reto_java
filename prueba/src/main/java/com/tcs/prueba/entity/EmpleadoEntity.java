package com.tcs.prueba.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import javax.persistence.*;

@Entity
@Table(name = "empleado")
@Data
public class EmpleadoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEmpleado;
    private String nombre;
    private String apellidos;
    private String numeroDocumento;
    private String correo;
    private String telefono;
    private boolean activo;
    private double salario;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IdDepartamento")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private DepartamentoEntity idDepartamento;
}
