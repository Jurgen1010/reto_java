package com.tcs.prueba.repository;

import com.tcs.prueba.entity.DepartamentoEntity;
import com.tcs.prueba.entity.FuncionesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FuncionesRepository extends JpaRepository<FuncionesEntity, Long> {

    public List<FuncionesEntity> findFuncionesByIdDepartamento(DepartamentoEntity idDepartamento);
}
