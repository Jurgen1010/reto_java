package com.tcs.prueba.repository;

import com.tcs.prueba.entity.EmpleadoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpleadoRepository extends JpaRepository<EmpleadoEntity, Long> {

    public EmpleadoEntity findByNumeroDocumento(String numeroDocumento);
}
